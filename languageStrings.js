var languageStrings = {
    "en": {
        "translation": {
            "FACTS": [
                "Eskimos have over 15 words for the English word of Snow.",
                "The world's youngest parents were 8 and 9 and lived in China in 1910.",
                "Every person has a unique tongue print.",
                "It is physically impossible for pigs to look up in the sky.",
                "The crocodile’s tongue is unmovable, as it is attached to the roof of its mouth.",
                "It is illegal to hunt camels in the state of Arizona.",
                "The King of Hearts is the only king in a deck of cards without a mustache.",
                "The opposite sides of a die will always add up to seven.",
                "No number from 1 to 999 includes the letter a in its word form.",
                "Cats can’t taste sweet things because of a genetic defect.",
                "Apple seeds contain cyanide.",
                "A Greek-Canadian man invented the Hawaiian pizza.",
                "The average person has four to six dreams a night.",
                "Dolphins give each other names.",
                "It’s possible to turn peanut butter into diamonds."
            ],
            "SKILL_NAME" : "Alexa Skill Stupid Facts",
            "GET_FACT_MESSAGE" : "Here's your fact: ",
            "HELP_MESSAGE" : "You can say tell me a stupid fact, or, you can say exit... What can I help you with?",
            "HELP_REPROMPT" : "What can I help you with?",
            "STOP_MESSAGE" : "Goodbye!"
        }
    }
};

module.exports = languageStrings;
